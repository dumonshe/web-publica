from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os

app = Flask(__name__)
app.secret_key = "super secret key"

@app.route('/')
def index():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		return render_template('home.html')

@app.route('/login', methods=['POST'])
def do_admin_login():
	if request.form['password'] == 'password' and request.form['username'] == 'admin':
		session['logged_in'] = True
		return render_template('home.html')	
	else:
		flash('wrong password!')
		return index()

@app.route('/home')
def home():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		return render_template('home.html')

@app.route('/caloventor')
def caloventor():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		return render_template('termostato.html')
	
@app.route('/about')
def about():
	return render_template('about.html')
	
@app.route('/showSignin')
def showSignin():
    return render_template('signin.html')


	
@app.route('/logout')
def logout():
	session.pop('username', None)
	session['logged_in'] = False
	return redirect('/')

if __name__ == '__main__':
	app.run(debug=True,host='0.0.0.0', port=4000)
